<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ProductController extends Controller
{

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        $now = Carbon::now();
        $products = Product::whereDate('expiration_date', '>', $now->toDateString())
            ->orderBy('views', 'desc')
            ->get();
        return view('product.index', compact('products'));
    }


    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function search(Request $request)
    {
        $search = $request->input('search');
        $products = Product::query()
            ->where('name', 'LIKE', "%{$search}%")
            ->orWhere('category', 'LIKE', "%{$search}%")
            ->get();

        return view('product.index', compact('products'));
    }


    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function searchByExpDate(Request $request)
    {
        $search = $request->input('search');
        $products = Product::query()
            ->where('expiration_date', 'LIKE', "%{$search}%")
            ->get();

        return view('product.index', compact('products'));
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function create()
    {
        return view('product.create');
    }


    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'image' => 'required|image',
            'category' => 'required',
            'contact_info' => 'required',
            'cuantity' => 'required',
            'price' => 'required',
            'expiration_date' => 'required'
        ]);

        $image = $request->image;
        $newImage = time() . $image->getClientOriginalName();
        $image->move('uploads/images', $newImage);

        $id = Auth::id();
        $post = Product::create([
            'user_id' => $id,
            'name' => $request->name,
            'image' => 'uploads/images/' . $newImage,
            'category' => $request->category,
            'contact_info' => $request->contact_info,
            'cuantity' => $request->cuantity,
            'price' => $request->price,
            'expiration_date' => $request->expiration_date,
            'sale1' => ($request->price) - ($request->price * 30) / 100,
            'sale2' => ($request->price) - ($request->price * 50) / 100,
            'sale3' => ($request->price) - ($request->price * 70) / 100,
        ]);

        return redirect()->route('products.index');
    }


    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function show($id)
    {
        $product = Product::find($id);
        $product->increment('views');
        $exp_date = Carbon::parse($product->expiration_date);
        $now = Carbon::now();
        $diff = $exp_date->diffInDays($now);

        return view('product.show')->with('product', $product)->with('diff', $diff);
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function edit($id)
    {
        $product = Product::where('id', $id)->where('user_id', Auth::id())->first();
        if ($product === null) {
            return redirect()->back();
        }
        return view('product.edit')->with('product', $product);
    }


    /**
     * Undocumented function
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $this->validate($request, [
            'name' => 'required',
            'category' => 'required',
            'contact_info' => 'required',
            'cuantity' => 'required',
            'price' => 'required',
            'expiration_date' => 'required'
        ]);


        if ($request->has('image')) {
            $image = $request->image;
            $newImage = time() . $image->getClientOriginalName();
            $image->move('uploads/images', $newImage);
            $product->image = 'uploads/images/' . $newImage;
        }


        $product->name = $request->name;
        $product->category = $request->category;
        $product->contact_info = $request->contact_info;
        $product->cuantity = $request->cuantity;
        $product->price = $request->price;
        $product->expiration_date = $request->expiration_date;
        $product->sale1 = ($request->price) - ($request->price * 30) / 100;
        $product->sale2 = ($request->price) - ($request->price * 50) / 100;
        $product->sale3 = ($request->price) - ($request->price * 70) / 100;
        $product->save();

        return redirect()->route('products.index');
    }


    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function destroy($id)
    {
        $product = Product::where('id', $id)->where('user_id', Auth::id())->first();
        if ($product === null) {
            return redirect()->back();
        }
        $product->delete();
        return redirect()->route('products.index');
    }
}
