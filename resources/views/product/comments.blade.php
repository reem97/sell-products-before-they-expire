@foreach ($comments as $item)
    <div @if ($item->parent_id != null) style ="margin-left:60px; color:red;" @endif>
        <strong>{{ $item->user->name }}</strong>
        <p>{{ $item->body }}</p>
        <form method="POST" action="{{ route('comments.store') }}">
            @csrf
            <div class="form-group">
                <textarea class="form-control" name="body" rows="3"></textarea>
                <input type="hidden" name="product_id" class="form-control" value="{{ $product_id }}">
                <input type="hidden" name="parent_id" class="form-control" value="{{ $item->id }}">
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Reply </button>
            </div>
        </form>
        @include('product.comments' , ['comments'=>$item->replies])
    </div>
@endforeach
