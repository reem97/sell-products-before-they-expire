@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="container">
                    <div class="card" style="width: 40rem;">
                        <img src="{{ URL::asset($product->image) }}" class="card-img-top" alt="{{ $product->image }}">
                        <div class="card-body">
                            <h5 class="card-title">{{ $product->name }}</h5>
                            <p class="card-text">category: {{ $product->category }}</p>
                            <p class="card-text">Contact Us: {{ $product->contact_info }}</p>
                            <p class="card-text">cuantity: {{ $product->cuantity }}</p>
                            @if ($diff > 40)
                                <p class="card-text">Price: {{ $product->price }}$</p>
                            @elseif ($diff >= 30)
                                <span>Price: <del>{{ $product->price }}$ </del></span>
                                <p class="card-text">After30%: {{ $product->sale1 }}$</p>
                            @elseif ($diff >= 15)
                                <span>Price: <del>{{ $product->price }}$ </del></span>
                                <p class="card-text">After50%: {{ $product->sale2 }}$</p>
                            @elseif ($diff < 15)
                                <span>Price: <del>{{ $product->price }}$ </del></span>
                                <p class="card-text">After70%: {{ $product->sale3 }}$</p>
                            @endif
                            <p class="card-text">expiration date: {{ $product->expiration_date }}</p>
                            <p class="card-text">Views: {{ $product->views }}</p>


                            <hr>

                            <h4>comments</h4>
                            @include('product.comments' , ['comments'=>$product->comments, 'product_id'=>$product->id])

                            <hr>

                            <form method="POST" action="{{ route('comments.store') }}">
                                @csrf
                                <div class="form-group">
                                    <textarea class="form-control" name="body" rows="3"></textarea>
                                    <input type="hidden" name="product_id" class="form-control"
                                        value="{{ $product->id }}">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Add Comment</button>
                                </div>
                            </form>

                            <hr>
                            <hr>
                            <a href="{{ route('products.index') }}" class="btn btn-primary">all products</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
