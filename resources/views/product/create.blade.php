@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
      <div class="col">
        <div class="container">
            <div class="jumbotron">
                <h1>Create Post</h1>
                <a href="{{route('products.index')}}" class="btn btn-primary">all products</a>
              </div>
        </div>
      </div>
    </div>
    <div class="row">
        @if (count($errors)>0)
        <ul>
            @foreach ($errors->all() as $item)
                <li>
                    {{$item}}
                </li>
            @endforeach
        </ul>
        @endif
      <div class="col">
        <div class="container">
            <form method="POST" action="{{route('products.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <label >Name</label>
                  <input type="text" name="name" class="form-control" >
                </div>
                <div class="form-group">
                    <label >Image</label>
                    <input type="file" name="image" class="form-control" >
                </div>
                <div class="form-group">
                    <label >Category</label>
                    <input type="text" name="category" class="form-control" >
                </div>
                <div class="form-group">
                    <label >Cuantity</label>
                    <input type="text" name="cuantity" class="form-control" >
                </div>
                <div class="form-group">
                    <label >Price</label>
                    <input type="text" name="price" class="form-control" >
                </div>
                <div class="form-group">
                    <label >Contact Us</label>
                    <input type="text" name="contact_info" class="form-control" >
                </div>
                <div class="form-group">
                    <label >Expiration Date</label>
                    <input type="date" name="expiration_date" class="form-control" >
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Save </button>
                </div>
              </form>
        </div>
      </div>
    </div>
</div>

@endsection
