@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
      <div class="col">
        <div class="container">
            <div class="jumbotron">
                <h1>Edit Post</h1>
                <a href="{{route('products.index')}}" class="btn btn-primary">all products</a>
              </div>
        </div>
      </div>
    </div>
    <div class="row">
        @if (count($errors)>0)
        <ul>
            @foreach ($errors->all() as $item)
                <li>
                    {{$item}}
                </li>
            @endforeach
        </ul>
        @endif
      <div class="col">
        <div class="container">
            <form method="POST" action="{{route('products.update',$product->id)}}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                  <label >Name</label>
                  <input type="text" name="name" value="{{$product->name}}" class="form-control" >
                </div>
                <div class="form-group">
                    <label >Image</label>
                    <input type="file" name="image" value="{{$product->image}}" class="form-control" >
                </div>
                <div class="form-group">
                    <label >Category</label>
                    <input type="text" name="category" value="{{$product->category}}" class="form-control" >
                </div>
                <div class="form-group">
                    <label >Cuantity</label>
                    <input type="text" name="cuantity" value="{{$product->cuantity}}" class="form-control" >
                </div>
                <div class="form-group">
                    <label >Price</label>
                    <input type="text" name="price" value="{{$product->price}}" class="form-control" >
                </div>
                <div class="form-group">
                    <label >Contact Info</label>
                    <input type="text" name="contact_info" value="{{$product->contact_info}}" class="form-control" >
                  </div>
                <div class="form-group">
                    <label >Expiration Date</label>
                    <input type="date" name="expiration_date" value="{{$product->expiration_date}}" class="form-control" readonly="readonly" >
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Update </button>
                </div>
              </form>
        </div>
      </div>
    </div>
</div>

@endsection
