@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col">
                <div class="container">
                    <div class="jumbotron">
                        <h1 class="display-4">All Products</h1>
                        <a href="{{ route('products.create') }}" class="btn btn-primary">Create Product</a>
                        <br>
                        <br>
                        <a href="{{ route('products.index') }}" class="btn btn-primary">All Product</a>
                        <br>
                        <br>
                        <div class="input-group">
                            <form action="{{ route('product.search') }}" method="GET">
                                <input type="text" name="search" placeholder="search by name or category" required />
                                <button type="submit" class="btn btn-primary">Search</button>
                            </form>
                        </div>
                        <br>
                        <div class="input-group">
                            <form action="{{ route('product.searchByExpDate') }}" method="GET">
                                <input type="date" name="search" placeholder="search by expiration date" required />
                                <button type="submit" class="btn btn-primary">Search</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @if ($products->count() > 0)
                <div class="col">
                    @php
                        $i = 0;
                    @endphp
                    <div class="container">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">name</th>
                                    <th scope="col">image</th>
                                    <th scope="col">category</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $item)
                                    <tr>
                                        <th scope="row">{{ ++$i }}</th>
                                        <td scope="col">{{ $item->name }}</td>
                                        <td scope="col">
                                            <!--  <img src="{{ '$item->photo' }}" alt="{{ $item->photo }}" class="img-tumbnail" width="50" height="50">-->
                                            <img src="{{ URL::asset($item->image) }}" alt="{{ $item->image }}"
                                                class="img-thumbnail" width="100" height="100">
                                        </td>
                                        <td scope="col"> {{ $item->category }}</td>
                                        <td scope="col">
                                            <a href="{{ route('products.show', $item->id) }}">show</a>&nbsp;&nbsp;
                                            @if ($item->user_id == Auth::id())
                                                <a href="{{ route('products.edit', $item->id) }}">edit</a>&nbsp;&nbsp;

                                                <div class="col-sm">
                                                    <form action="{{ route('products.destroy', $item->id) }}"
                                                        method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            @else
                <div class="col">
                    <div class="alert alert-danger" role="alert">
                        No Products
                    </div>
                </div>
            @endif
        </div>




    </div>

@endsection
